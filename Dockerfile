FROM ubuntu as builder
RUN apt update && \
    apt install -y wget unzip && \
    wget https://www.tecomat.cz/modules/DownloadManager/download.php?alias=plccoms_v61 -O PLCComS.zip && \
    mkdir app && \
    cd app && \
    unzip ../PLCComS.zip

FROM alpine
USER root
RUN apk add --no-cache libc6-compat libstdc++ libgcc libssl1.1 gcompat && \
    adduser -S -u 1001 runnerusr
USER 1001
EXPOSE 5010
WORKDIR /app
CMD ["./PLCComS.sh", "-l", "/dev/stdout"]
ENTRYPOINT []
COPY --from=builder --chown=1001:root /app /app
